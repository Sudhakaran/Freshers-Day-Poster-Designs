# Fresher's Day Computer Science Department Posters 2017
[![PSVersion](https://img.shields.io/badge/Photoshop%20Version-CC%202017-blue.svg)](https://en.wikipedia.org/wiki/Adobe_Photoshop#CC_2017) [![License](https://img.shields.io/badge/license-(CC)%20BY-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/legalcode)

These are the PSD files for the Computer Science Departmental Posters designed for the fresher's day 2017.

## Posters:

CS Poster 1
![CS poster 1](CS-Poster-1-image.jpg )

CS Poster 2
![CS poster 2](CS-Poster-2-image.jpg )

Arivu Poster
![Arivu Poster](Arivu-Poster-image.jpg)